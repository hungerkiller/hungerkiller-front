import React from "react";
import ReactDOM from "react-dom";
import { Route, BrowserRouter as Router } from 'react-router-dom'
import App from "./App";
import LogIn from "./LogIn"
import "./index.css";
import history from './history';
import axios from 'axios';
import RestaurantsFolder from "./RestaurantsFolder"
import AddRestaurant from "./AddRestaurant";
import Restaurant from "./Restaurant";
import EditRestaurant from "./EditRestaurant";
import Register from "./Register";
import Deliverer from "./Deliverer";
import EditDeliverer from "./EditDeliverer";
import AddRestaurantDishes from "./AddRestaurantDish";
import DishesFolder from "./DishesFolder";
import EditDish from "./EditDish";
import AddWorkHours from "./AddWorkHours";
import WorkHours from "./WorkHours";
import JobOffers from "./JobOffers";
import CurrentOrders from "./CurrentOrders";
import RestaurantsOrders from "./RestaurantsOrders"
import OrdersFolder from "./OrdersFolder";
import AddNewOrder from "./AddNewOrder";

axios.defaults.headers.common['Authorization'] = localStorage.getItem('Authorization');

const routing = (
  <Router history={history}>
    <div>
      <Route exact path="/" component={App} />
      <Route exact path="/login" component={LogIn} />
      <Route path="/register" component={Register} />
      <Route path="/restaurants" component={RestaurantsFolder} />
      <Route path="/add-restaurant" component={AddRestaurant} />
      <Route path="/restaurant/:id" component={Restaurant} />
      <Route path="/edit-restaurant/:id" component={EditRestaurant} />
      <Route path="/restaurant-add-dish/:id" component={AddRestaurantDishes} />
      <Route path="/deliverer" component={Deliverer} />
      <Route path="/edit-deliverer" component={EditDeliverer} />
      <Route path="/dishes" component={DishesFolder} />
      <Route path="/edit-dish/:id" component={EditDish} />
      <Route path="/edit-workhours/:id/:dayOfWeek/:timeStart/:timeEnd" component={WorkHours} />
      <Route path="/add-workhours" component={AddWorkHours} />
      <Route path="/job-offers" component={JobOffers} />
      <Route path="/current-orders" component={CurrentOrders} />
      <Route path="/restaurant-orders" component={RestaurantsOrders} />
      <Route path="/orders" component={OrdersFolder} />
      <Route path="/new-order/:id" component={AddNewOrder} />
    </div>
  </Router>
)

ReactDOM.render(routing, document.getElementById('root'));
