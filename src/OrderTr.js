import "semantic-ui-css/semantic.min.css";
import React, {Component} from 'react';

class CurrentOrderTr extends Component {
    state = { userActivities: [], isClicked: false }

    createBlocks = () =>{
        const blocks = this.props.orders.map((act) => {
            return (
                <tr
                    role="button" style={{ display: "none", cursor: "pointer" }}>
                    <td><div><label class="var">{act['id']}</label></div></td>
                    <td><div><label class="var">{act['user']['deliveryAddress']['street']} - {act['user']['deliveryAddress']['town']}</label></div></td>
                    <td><div><label className="var">{act['status']}</label></div></td>
                    <td><div><label className="var">{act['delivererId']}</label></div></td>
                </tr>
            )
        })
        return blocks
    }

    render() {
        return (
            this.createBlocks()
        )
    }
}

export default CurrentOrderTr
