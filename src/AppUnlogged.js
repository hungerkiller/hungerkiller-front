import React from "react";
import "semantic-ui-css/semantic.min.css";
import { Grid } from "semantic-ui-react";
import SideMenuUnlogged from "./components/SideMenuUnlogged";

function AppUnlogged() {
    return (
        <div style={{ height: "100%" }}>
            <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
                <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
                    <SideMenuUnlogged />
                </Grid.Column>
                <Grid.Column stretched width={14}>
                    <div>
                        <h1>Hunger Killer</h1>
                        <hr />
                        <p>
                            <h4>Welcome to the website that will most effectively help you murder your hunger!</h4>
                            <h4>Log in to have access to all portal functions.</h4>
                        </p>
                        <hr />
                        <h2>Do you know that?</h2>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris varius dui vitae viverra tempus. Phasellus pulvinar, neque eget vestibulum dictum, turpis nulla ornare nunc, in hendrerit quam ante nec velit. Nam vestibulum egestas consequat. Fusce placerat tincidunt arcu, eget sollicitudin libero maximus a. Sed interdum turpis eget eros tempor volutpat a id mi. Curabitur lacus sapien, faucibus quis elit lobortis, tristique vestibulum elit. In sed pellentesque quam, ut pharetra lacus. Etiam sollicitudin leo nec convallis fringilla. Sed accumsan in mauris at efficitur. Maecenas quam ligula, sagittis a feugiat pharetra, gravida non lorem. Quisque tincidunt euismod nunc eu tempor. Integer commodo ornare urna, eget feugiat purus fermentum et. Proin ultrices pharetra urna, vel sollicitudin tortor finibus in.
                            Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas commodo at lacus id elementum. Morbi pretium quis orci eu rhoncus. Mauris convallis elementum tortor, at viverra sem convallis ut. Morbi porta lectus in purus rutrum, at suscipit odio tincidunt. Sed pellentesque ac augue sit amet ultrices. Sed ultrices molestie mauris, eget pharetra elit facilisis interdum. Nunc accumsan leo faucibus libero luctus tristique. Donec id lectus ac tellus sollicitudin facilisis quis a justo. Nulla semper sem ac nunc accumsan vulputate. Vivamus placerat, nibh vel suscipit venenatis, mi mauris convallis nibh, scelerisque condimentum diam risus quis nisi. Integer vel cursus diam, ac luctus orci.
                            Phasellus bibendum augue a egestas molestie. Vestibulum ut porta nibh, nec accumsan augue. Praesent vel massa diam. Donec et sapien ac lacus ultrices faucibus vel quis lacus. Etiam mauris sapien, laoreet ac viverra vitae, venenatis et est. Aenean ullamcorper ipsum quis rutrum tincidunt. Suspendisse potenti. Sed malesuada leo risus, sit amet viverra lectus bibendum eu. Quisque condimentum placerat nibh, sed malesuada ante auctor a. Sed dignissim mauris ante, iaculis aliquet sem lacinia vitae. Nulla vehicula metus nisl.
                            Fusce varius lobortis est. Vestibulum ornare mollis arcu, non molestie justo facilisis a. Pellentesque nec facilisis odio. Vivamus ornare orci sed dui viverra volutpat. In ac purus nulla. Nam finibus ac risus vel scelerisque. Phasellus massa quam, euismod sed elit ut, elementum vulputate dui. Aenean in justo eget eros ornare placerat facilisis et purus. Donec vel risus facilisis nunc elementum congue nec ac leo. Sed massa justo, rutrum in facilisis accumsan, eleifend sed felis. In hac habitasse platea dictumst. Ut porttitor lorem vitae dolor molestie ullamcorper.
                            Integer at tempor mauris. Phasellus vel bibendum ex. In elementum facilisis orci, quis congue ligula finibus in. Ut ac justo sed erat malesuada scelerisque. Sed id purus sagittis, aliquam purus sed, venenatis enim. Duis maximus felis ac finibus auctor. Nulla vehicula sem maximus nisi suscipit pretium. Nulla feugiat arcu vel ex imperdiet faucibus ut sit amet nibh. Quisque pellentesque finibus pretium. Quisque sed facilisis odio, eget feugiat ex. Quisque faucibus, eros a lobortis auctor, orci neque condimentum erat, in hendrerit lacus diam vitae lectus. Mauris quis mi euismod mi sagittis iaculis eget eu sem. Nunc viverra sed ipsum et euismod. Phasellus imperdiet eros quis purus luctus porta.
                        </p>
                    </div>
                </Grid.Column>
            </Grid>
        </div>
    );
}

export default AppUnlogged;
