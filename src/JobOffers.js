import "semantic-ui-css/semantic.min.css";
import SideMenu from "./components/SideMenu";
import { Grid } from "semantic-ui-react";
import React, { Component } from 'react';
import axios from 'axios'
import AppUnlogged from "./AppUnlogged";
import { Link } from 'react-router-dom'
import JobOfferTr from "./JobOfferTr";

class JobOffers extends Component {
    constructor(props) {
        super(props);
        this.state = { show: false, jobOffers: [], permissons: true, success: false, error: false }
    }

    componentDidMount() {
        var self = this;

        var axios = require('axios');
        var config = {
            method: 'get',
            url: 'http://localhost:8080/hungerkiller/v1/deliverer/jobOffers',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('Authorization')
            }
        };

        axios(config)
            .then(function (response) {
                // console.log(JSON.stringify(response.data));
                self.setState({
                    jobOffers: response.data,
                })
            })
            .catch(function (error) {
                // console.log(error);
            });
    }

    onSubmit = e => {
    }

    render() {
        if (!axios.defaults.headers.common["Authorization"]) {
            return (<AppUnlogged />);
        }
        return (
            <div style={{ height: "100%" }}>
                <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
                    <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
                        <SideMenu />
                    </Grid.Column>
                    <Grid.Column stretched width={14}>
                        <div>
                            <Link to="/">
                                <button class="ui labeled icon button">
                                    <i class="caret left icon"/> Back to main page
                                </button>
                            </Link>
                            <hr />
                            <Grid columns={1} style={{ marginTop: 15 }}>
                                <Grid.Column>
                                    <div style={{ display: this.state.success ? "none" : "block" }}>
                                        <h3>Job offers (click to handle order)</h3>
                                        <hr />
                                        <Grid columns={1}>
                                            <Grid.Column>
                                                <table className="ui sortable celled table">
                                                    <thead>
                                                        <tr>
                                                            <th>Id of order</th>
                                                            <th>Name of restaurant</th>
                                                            <th>Restaurant address</th>
                                                            <th>User address</th>
                                                            <th>Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <JobOfferTr currentOrders={this.state.jobOffers} />
                                                    </tbody>
                                                </table>
                                            </Grid.Column>
                                        </Grid>
                                    </div>
                                </Grid.Column>
                            </Grid>
                        </div>
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}

export default JobOffers;
