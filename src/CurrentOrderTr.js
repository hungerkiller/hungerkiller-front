import "semantic-ui-css/semantic.min.css";
import React, {Component} from 'react';

class CurrentOrderTr extends Component {
    state = { userActivities: [], isClicked: false }

    onClick(act) {
        var axios = require('axios');
        var newAct = act
        newAct['status'] = 'done'

        var config = {
            method: 'put',
            url: 'http://localhost:8080/hungerkiller/v1/order',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('Authorization')
            },
            data: newAct
        };

        axios(config)
            .then(function (response) {
                // console.log(JSON.stringify(response.data));
                window.location.reload();
            })
            .catch(function (error) {
                // console.log(error);
            });
    }

    createBlocks = () =>{
        const blocks = this.props.currentOrders.map((act) => {
            return (
                <tr
                    role="button" onClick={() => this.onClick(act)} style={{ cursor: "pointer" }}>
                    <td><div><label class="var">{act['id']}</label></div></td>
                    <td><div><label class="var">{act['restaurant']['name']}</label></div></td>
                    <td><div><label class="var">{act['restaurant']['address']['street']} - {act['restaurant']['address']['town']}</label></div></td>
                    <td><div><label class="var">{act['user']['deliveryAddress']['street']} - {act['user']['deliveryAddress']['town']}</label></div></td>
                    <td><div><label className="var">{act['status']}</label></div></td>
                </tr>
            )
        })
        return blocks
    }

    render() {
        return (
            this.createBlocks()
        )
    }
}

export default CurrentOrderTr
