import "semantic-ui-css/semantic.min.css";
import SideMenu from "./components/SideMenu";
import { Form, Grid } from "semantic-ui-react";
import React, { Component } from 'react';
import axios from 'axios'
import AppUnlogged from "./AppUnlogged";
import {Link} from "react-router-dom";
import history from "./history";

class AddRestaurantDish extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            id: 0,
            name: "",
            description: "",
            price: 0.0,
            permissons: true,
            success: false,
            error: false
        }
    }

    componentDidMount() {
        const { id } = this.props.match.params
        this.setState({id: id})

    }

    onSubmit = e => {
        const { id } = this.props.match.params
        var self = this;

        if(self.state.name === "" || self.state.price === ""){
            self.setState({ error: true, success: false })
            return;
        }

        var axios = require('axios');
        var data = JSON.stringify({
            "name": this.state.name,
            "description": this.state.description,
            "price": this.state.price
        });

        var config = {
            method: 'post',
            url: 'http://localhost:8080/hungerkiller/v1/restaurant/id/' + id + '/dish',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('Authorization')
            },
            data : data
        };

        axios(config)
            .then(function (response) {
                // console.log(JSON.stringify(response.data));
                self.setState({ success: true, error: false })
                history.replace("/restaurant/" + id);
                window.location.reload();
            })
            .catch(function (error) {
                self.setState({ error: true, success: false })
                // console.log(error);
            });
    }

    render() {
        if (!axios.defaults.headers.common["Authorization"]) {
            return (<AppUnlogged />);
        }
        return (
            <div style={{ height: "100%" }}>
                <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
                    <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
                        <SideMenu />
                    </Grid.Column>
                    <Grid.Column stretched width={14}>
                        <div>
                            <Link to={"/restaurant/" + this.state.id}>
                                <button class="ui labeled icon button">
                                    <i class="caret left icon"/> Back to restaurant
                                </button>
                            </Link>
                            <h1>New dishes to restaurant</h1>
                            <hr />
                            <Grid columns={2}>
                                <Grid.Column>
                                    <p style={{ display: this.state.success ? "block" : "none", color: "green" }}>SUCCESS!</p>
                                    <p style={{ display: this.state.error ? "block" : "none", color: "red" }}>Incorrect values.</p>
                                   <Form style={{ display: this.state.permissons ? "block" : "none" }} onSubmit={this.onSubmit}>
                                        <div style={{ backgroundColor: "#E9E9E9", borderRadius: 15, padding: 10}} >
                                            <div class="inline field" align="right">
                                                <label >Name:</label>
                                                <input  style={{ width: "80%" }} onChange={e => this.setState({ name: e.target.value })} placeholder='' />
                                            </div>
                                            <div className="inline field" align="right">
                                                <label >Price:</label>
                                                <input  style={{ width: "80%" }} onChange={e => this.setState({ price: e.target.value })} placeholder='' />
                                            </div>
                                            <div className="inline field" style={{marginRight: 50, marginLeft: 50,}}>
                                                <label>Description:</label>
                                                <textarea onChange={e => this.setState({description: e.target.value})}/>
                                            </div>
                                        </div>
                                        <div align="right" style={{ marginTop: 10 }} >
                                            <button class="circular ui icon button" style={{ backgroundColor: "#FFABB6" }} type="reset" >
                                                <i class="minus icon"/>
                                            </button>
                                            <label style={{ marginRight: 10 }}> Reset </label>
                                            <button class="circular ui icon button" style={{ backgroundColor: "#B2E8C4" }} type='submit' >
                                                <i class="check icon"/>
                                            </button>
                                            <label>Save dish</label>
                                        </div>
                                    </Form>
                                </Grid.Column>
                            </Grid>
                        </div>
                    </Grid.Column>
                </Grid>
            </div >
        );
    }
}
export default AddRestaurantDish;
