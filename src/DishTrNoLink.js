import React from 'react'
import "./css/RestaurantTr.css";

class DishTrNoLink extends React.Component {
    state = { userActivities: [] }

    onClick(id) {
        window.location.href = "restaurant/" + id;
    }

    createBlocks = () => {
        const blocks = this.props.dishes.map((act) => {
            return (
                <tr>
                    <td>{act["name"]}</td>
                    <td>{act["description"]}</td>
                    <td>{act["price"]}</td>
                </tr>
            )
        })
        return blocks
    }

    render() {
        return (
            this.createBlocks()
        )
    }
}

export default DishTrNoLink
