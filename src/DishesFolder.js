import "semantic-ui-css/semantic.min.css";
import SideMenu from "./components/SideMenu";
import { Grid } from "semantic-ui-react";
import React, { Component } from 'react';
import axios from 'axios'
import DishTr from "./DishTr";
import SideMenuUnlogged from "./components/SideMenuUnlogged";
import DishTrNoLink from "./DishTrNoLink";

class DishesFolder extends Component {
    state = { dishes: [], name: "", update: false }

    componentDidMount() {
        var self = this;
        var axios = require('axios');

        var config = {
            method: 'get',
            url: 'http://localhost:8080/hungerkiller/v1/dish',
            headers: { }
        };

        axios(config)
            .then(function (response) {
                // console.log(response.data);
                self.setState({ dishes: response.data})
            })
            .catch(function (error) {
            });
    }

    componentDidUpdate() {
        if (this.state.update) {
            var self = this;
            var axios = require('axios');

            var config = {
                method: 'get',
                url: 'http://localhost:8080/hungerkiller/v1/dish/name/' + self.state["name"],
                headers: {}
            };

            var config2 = {
                method: 'get',
                url: 'http://localhost:8080/hungerkiller/v1/dish',
                headers: { }
            };

            if(self.state["name"] !== "") {
                axios(config)
                    .then(function (response) {
                        self.setState({dishes: response.data, update: false})
                    })
                    .catch(function (error) {
                    });
            }else{
                axios(config2)
                    .then(function (response) {
                        self.setState({dishes: response.data, update: false})
                    })
                    .catch(function (error) {
                    });
            }
        }
    }

    render() {
        var DT;
        var Menu;
        if (!axios.defaults.headers.common["Authorization"]) {
            DT = <DishTrNoLink dishes={this.state.dishes}/>
            Menu = <SideMenuUnlogged/>
        }else{
            DT = <DishTr dishes={this.state.dishes}/>
            Menu = <SideMenu/>
        }

        return (
            <div style={{ height: "100%" }}>
                <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
                    <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
                        {Menu}
                    </Grid.Column>
                    <Grid.Column stretched width={14}>
                        <div>
                            <h3>Dishes</h3>
                            <hr/>
                            <Grid columns={1}>
                                <Grid.Column>
                                    <table className="ui sortable celled table">
                                        <thead>
                                        <tr>
                                            <th colSpan={5}>
                                                <div className="ui category search">
                                                    <div className="ui icon input" style={{width: "100%"}}>
                                                        <input className="prompt" type="text"
                                                               placeholder="Szukaj po nazwie..."
                                                               onChange={e => this.setState({
                                                                   name: e.target.value,
                                                                   update: true
                                                               })}/>
                                                        <i className="search icon"/>
                                                    </div>
                                                    <div className="results"/>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Price</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            { DT }
                                        </tbody>
                                    </table>
                                </Grid.Column>
                            </Grid>
                        </div>
                    </Grid.Column>
                </Grid>
            </div >
        );
    }
}

export default DishesFolder;
