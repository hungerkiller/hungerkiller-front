import "semantic-ui-css/semantic.min.css";
import SideMenu from "./components/SideMenu";
import { Form, Grid } from "semantic-ui-react";
import React, { Component } from 'react';
import axios from 'axios'
import AppUnlogged from "./AppUnlogged";
import { Link } from 'react-router-dom'
import WorkHoursTr from "./WorkHoursTr";


class Deliverer extends Component {
    constructor(props) {
        super(props);
        this.state = { show: false, serviceInCity: "", workHours: [], permissons: true, success: false, error: false }
    }

    componentDidMount() {
        var self = this;

        var axios = require('axios');
        var config = {
            method: 'get',
            url: 'http://localhost:8080/hungerkiller/v1/deliverer',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('Authorization')
            }
        };

        axios(config)
            .then(function (response) {
                // console.log(JSON.stringify(response.data));
                self.setState({
                    serviceInCity: response.data["serviceInCity"],
                    workHours: response.data["schedule"]
                })
            })
            .catch(function (error) {
                // console.log(error);
            });
    }

    onSubmit = e => {
    }

    render() {
        if (!axios.defaults.headers.common["Authorization"]) {
            return (<AppUnlogged />);
        }
        return (
            <div style={{ height: "100%" }}>
                <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
                    <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
                        <SideMenu />
                    </Grid.Column>
                    <Grid.Column stretched width={14}>
                        <div>
                            <Link to="/">
                                <button class="ui labeled icon button">
                                    <i class="caret left icon"/> Back to main page
                                </button>
                            </Link>
                            <hr />
                            <Grid columns={1} style={{ marginTop: 15 }}>
                                <Grid.Column>
                                    <Form style={{ display: this.state.success ? "none" : "block" }} onSubmit={this.onSubmit}>
                                        <div style={{ backgroundColor: "#E9E9E9", borderRadius: 15, padding: 20 }} >
                                            <Grid columns={2}>
                                                <Grid.Row>
                                                    <Grid.Column>
                                                        <div class="inline field">
                                                            <label >City:</label>
                                                            <label style={{ color: "#918383" }}>{this.state.serviceInCity}</label>
                                                        </div>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <div align="right">
                                                            <label style={{ marginRight: 10 }}>EDIT</label>
                                                            <Link to={"/edit-deliverer/"}>
                                                                <button className="circular ui icon button"
                                                                    style={{ backgroundColor: "#B2E8C4" }}
                                                                    type='button'>
                                                                    <i className="check icon"></i>
                                                                </button>
                                                            </Link>
                                                        </div>
                                                    </Grid.Column>
                                                </Grid.Row>
                                            </Grid>
                                        </div>
                                    </Form>
                                </Grid.Column>
                                <Grid.Column>
                                    <div style={{ display: this.state.success ? "none" : "block" }}>
                                        <h3>Workhours</h3>
                                        <hr />
                                        <Grid columns={1}>
                                            <Grid.Column>
                                                <table className="ui sortable celled table">
                                                    <thead>
                                                        <tr>
                                                            <th>Day of week</th>
                                                            <th>Time start</th>
                                                            <th>Time end</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <WorkHoursTr workHours={this.state.workHours} />
                                                    </tbody>
                                                </table>
                                                <div align="right" style={{ marginTop: 10 }}>
                                                    <Link to="/add-workhours">
                                                        <button className="circular ui icon button" style={{ backgroundColor: "#B2E8C4" }}
                                                            type="reset">
                                                            <i className="plus icon"/>
                                                        </button>
                                                        <label>Add work hours</label>
                                                    </Link>
                                                </div>

                                            </Grid.Column>
                                        </Grid>
                                    </div>
                                </Grid.Column>
                            </Grid>
                        </div>
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}

export default Deliverer;
