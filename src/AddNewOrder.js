import "semantic-ui-css/semantic.min.css";
import React, { Component } from 'react';
import axios from 'axios'
import AppUnlogged from "./AppUnlogged";
import history from "./history";

class AddNewOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            name: "",
            street: "",
            town: "",
            zipcode: "",
            description: "",
            openFromTime: "",
            openTillTime: "",
            phoneNumber: "",
            permissons: true,
            uccess: false,
            error: false
        }
    }

    componentDidMount(key, value) {
        const { id } = this.props.match.params

        var ordersIds = JSON.parse(localStorage.getItem("ordersIds"));
        if(ordersIds.length === 0){
            history.replace("/orders");
            window.location.reload();
            return;
        }
        var dishes = []
        for (const x in ordersIds){
            dishes.push({"id": ordersIds[x]})
        }

        var axios = require('axios');
        var data = JSON.stringify({
            "restaurant": {
                "id": id
            },
            "dishes": dishes,
            "user": {
                "id": localStorage.getItem("Id"),
                "role": "customer"
            }
        });

        var config = {
            method: 'post',
            url: 'http://localhost:8080/hungerkiller/v1/order',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('Authorization')
            },
            data : data
        };

        axios(config)
            .then(function (response) {
                console.log(JSON.stringify(response.data));
                localStorage.setItem("ordersIds", JSON.stringify([]))
                history.replace("/orders");
                window.location.reload();
            })
            .catch(function (error) {
                console.log(error);
            });


    }

    render() {
        if (!axios.defaults.headers.common["Authorization"]) {
            return (<AppUnlogged />);
        }
        return (<div/>);
    }
}

export default AddNewOrder;
