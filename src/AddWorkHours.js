import "semantic-ui-css/semantic.min.css";
import SideMenu from "./components/SideMenu";
import { Form, Grid } from "semantic-ui-react";
import React, { Component } from 'react';
import axios from 'axios'
import AppUnlogged from "./AppUnlogged";
import Select from "react-select";

const days = [
    { value: 'monday', label: 'Monday' },
    { value: 'tuesday', label: 'Tuesday' },
    { value: 'wednesday', label: 'Wednesday' },
    { value: 'thursday', label: 'Thursday' },
    { value: 'friday', label: 'Friday' },
    { value: 'saturday', label: 'Saturday' },
    { value: 'sunday', label: 'Sunday' }
]

class AddWorkHours extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            dayOfWeek: "",
            timeStart: "",
            timeEnd: "",
            permissons: true,
            success: false,
            error: false
        }
    }

    componentDidMount() {
    }

    onSubmit = e => {
        var self = this;
        var axios = require('axios');
        var data = JSON.stringify({
            "dayOfWeek": this.state.dayOfWeek,
            "timeStart": this.state.timeStart,
            "timeEnd": this.state.timeEnd
        });

        var config = {
            method: 'post',
            url: 'http://localhost:8080/hungerkiller/v1/deliverer/workHours',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('Authorization')
            },
            data: data
        };

        axios(config)
            .then(function (response) {
                // console.log(JSON.stringify(response.data));
                self.setState({ success: true, error: false })
            })
            .catch(function (error) {
                self.setState({ error: true, success: false })
                // console.log(error);
            });
    }

    render() {
        if (!axios.defaults.headers.common["Authorization"]) {
            return (<AppUnlogged />);
        }
        return (
            <div style={{ height: "100%" }}>
                <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
                    <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
                        <SideMenu />
                    </Grid.Column>
                    <Grid.Column stretched width={14}>
                        <div>
                            <h1>New work hours</h1>
                            <hr />
                            <Grid columns={2}>
                                <Grid.Column>
                                    <p style={{ display: this.state.success ? "block" : "none", color: "green" }}>SUCCESS!</p>
                                    <p style={{ display: this.state.error ? "block" : "none", color: "red" }}>Incorrect values.</p>
                                   <Form style={{ display: this.state.permissons ? "block" : "none" }} onSubmit={this.onSubmit}>
                                        <div style={{ backgroundColor: "#E9E9E9", borderRadius: 15, padding: 10 }} >
                                           <div class="inline field" align="right">
                                                <label >Day of week:</label>
                                               <Select options={days} placeholder="MONDAY"
                                                       onChange={e => this.setState({dayOfWeek: e.value})}/>
                                            </div>
                                            <div className="inline field" align="right">
                                                <label>Time start:</label>
                                                <input style={{ width: "80%" }} onChange={e => this.setState({ timeStart: e.target.value })}
                                                    placeholder='' />
                                            </div>
                                            <div className="inline field" align="right">
                                                <label>Time end:</label>
                                                <input style={{ width: "80%" }} onChange={e => this.setState({ timeEnd: e.target.value })}
                                                    placeholder='' />
                                            </div>
                                        </div>
                                        <div align="right" style={{ marginTop: 10 }} >
                                            <button class="circular ui icon button" style={{ backgroundColor: "#FFABB6" }} type="reset" >
                                                <i class="minus icon"/>
                                            </button>
                                            <label style={{ marginRight: 10 }}> Reset </label>
                                            <button class="circular ui icon button" style={{ backgroundColor: "#B2E8C4" }} type='submit' >
                                                <i class="check icon"/>
                                            </button>
                                            <label>Save work hours</label>
                                        </div>
                                    </Form>
                                </Grid.Column>
                            </Grid>
                        </div>
                    </Grid.Column>
                </Grid>
            </div >
        );
    }
}

export default AddWorkHours;
