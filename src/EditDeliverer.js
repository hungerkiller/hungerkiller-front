import "semantic-ui-css/semantic.min.css";
import SideMenu from "./components/SideMenu";
import { Form, Grid } from "semantic-ui-react";
import React, { Component } from 'react';
import axios from 'axios'
import AppUnlogged from "./AppUnlogged";

class EditDeliverer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            serviceInCity: "",
            delivererId: "",
            workHours: [],
            permissons: true,
            success: false,
            error: false }
    }

    componentDidMount() {
        var self = this;

        var axios = require('axios');
        var config = {
            method: 'get',
            url: 'http://localhost:8080/hungerkiller/v1/deliverer',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('Authorization')
            }
        };

        axios(config)
            .then(function (response) {
                // console.log(JSON.stringify(response.data));
                self.setState({
                    delivererId: response.data["id"],
                    serviceInCity: response.data["serviceInCity"],
                    workHours: response.data["schedule"]
                })
            })
            .catch(function (error) {
                // console.log(error);
            });
    }

    onSubmit = e => {
        var self = this;
        var axios = require('axios');
        var data = JSON.stringify({
            "id": this.state.delivererId,
            "serviceInCity": this.state.serviceInCity
        });

        var config = {
            method: 'put',
            url: 'http://localhost:8080/hungerkiller/v1/deliverer',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('Authorization')
            },
            data: data
        };

        axios(config)
            .then(function (response) {
                // console.log(JSON.stringify(response.data));
                self.setState({ success: true, error: false })
            })
            .catch(function (error) {
                self.setState({ error: true, success: false })
                // console.log(error);
            });
    }

    render() {
        if (!axios.defaults.headers.common["Authorization"]) {
            return (<AppUnlogged />);
        }
        return (
            <div style={{ height: "100%" }}>
                <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
                    <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
                        <SideMenu />
                    </Grid.Column>
                    <Grid.Column stretched width={14}>
                        <div>
                            <h1>Editing deliverer info</h1>
                            <hr />
                            <Grid columns={2}>
                                <Grid.Column>
                                    <p style={{ display: this.state.success ? "block" : "none", color: "green" }}>SUCCESS!</p>
                                    <p style={{ display: this.state.error ? "block" : "none", color: "red" }}>Incorrect values.</p>
                                    <Form style={{ display: this.state.permissons ? "block" : "none" }} onSubmit={this.onSubmit}>
                                        <div style={{ backgroundColor: "#E9E9E9", borderRadius: 15, padding: 10 }} >
                                            <div class="inline field" align="right">
                                                <label >City:</label>
                                                <input style={{ width: "80%" }} onChange={e => this.setState({ serviceInCity: e.target.value })} value={this.state.serviceInCity} />
                                            </div>
                                        </div>
                                        <div align="right" style={{ marginTop: 10 }} >
                                            <button class="circular ui icon button" style={{ backgroundColor: "#FFABB6" }} type="reset" >
                                                <i class="minus icon"/>
                                            </button>
                                            <label style={{ marginRight: 10 }}> Reset </label>
                                            <button class="circular ui icon button" style={{ backgroundColor: "#B2E8C4" }} type='submit' >
                                                <i class="check icon"/>
                                            </button>
                                            <label>Save information</label>
                                        </div>
                                    </Form>
                                </Grid.Column>
                            </Grid>
                        </div>
                    </Grid.Column>
                </Grid>
            </div >
        );
    }
}

export default EditDeliverer;
