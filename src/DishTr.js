import React from 'react'
import "./css/RestaurantTr.css";
import {Link} from "react-router-dom";

class DishTr extends React.Component {
    state = { userActivities: [] }

    onClick(id) {
        window.location.href = "restaurant/" + id;
    }

    createBlocks = () => {
        const blocks = this.props.dishes.map((act) => {
            return (
                <tr>
                    <td><Link to={"/restaurant/" + act["restaurant"]["id"]}>{act["name"]}</Link></td>
                    <td><Link to={"/restaurant/" + act["restaurant"]["id"]}>{act["description"]}</Link></td>
                    <td><Link to={"/restaurant/" + act["restaurant"]["id"]}>{act["price"]}</Link></td>
                </tr>
            )
        })
        return blocks
    }

    render() {
        return (
            this.createBlocks()
        )
    }
}

export default DishTr
