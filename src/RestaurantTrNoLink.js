import React from 'react'
import "./css/RestaurantTr.css";

class RestaurantTrNoLink extends React.Component {
    state = { userActivities: [] }

    createBlocks = () => {
        const blocks = this.props.restaurants.map((act) => {
            return (
                <tr>
                    <td>
                        <div><label class="var">{act['name']}</label></div>
                    </td>
                    <td>{act['openFromTime']} - {act['openTillTime']}</td>
                    <td>{act['address']['street']}, {act['address']['zipcode']} {act['address']['town']}</td>
                    <td>{act['phoneNumber']}</td>
                    <td>{act['description']}</td>
                </tr>
            )
        })
        return blocks
    }

    render() {
        return (
            this.createBlocks()
        )
    }
}

export default RestaurantTrNoLink
