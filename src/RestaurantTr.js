import React from 'react'
import { Link } from 'react-router-dom'
import "./css/RestaurantTr.css";

class RestaurantTr extends React.Component {
    state = { userActivities: [] }

    onClick(id) {
        window.location.href = "restaurant/" + id;
    }

    createBlocks = () => {
        const blocks = this.props.restaurants.map((act) => {
            return (
                <tr role="button" onClick={() => this.onClick(act['id'])} style={{ cursor: "pointer" }}>
                    <td>
                        <Link to={act['id']}>
                            <div><label class="var">{act['name']}</label></div>
                        </Link>
                    </td>
                    <td><Link to={act['id']}>{act['openFromTime']} - {act['openTillTime']}</Link></td>
                    <td><Link to={act['id']}>{act['address']['street']}, {act['address']['zipcode']} {act['address']['town']}</Link></td>
                    <td><Link to={act['id']}>{act['phoneNumber']}</Link></td>
                    <td><Link to={act['id']}>{act['description']}</Link></td>
                </tr>
            )
        })
        return blocks
    }

    render() {
        return (
            this.createBlocks()
        )
    }
}

export default RestaurantTr
