import React from 'react'
import "./css/RestaurantTr.css";

class WorkHoursTr extends React.Component {
    state = { userActivities: [] }

    onClick(id, dayOfWeek, startTime, endTime) {
        window.location.href = "edit-workhours/" + id + '/' + dayOfWeek + '/' + startTime + '/' + endTime;
    }

    createBlocks = () => {
        const blocks = this.props.workHours.map((act) => {
            return (
                <tr
                >
                    <td role="button" onClick={() => this.onClick(act['id'], act['dayOfWeek'], act['timeStart'], act['timeEnd'])}
                        style={{ cursor: "pointer" }}><div contenteditable>{act["dayOfWeek"]}</div></td>
                    <td role="button" onClick={() => this.onClick(act['id'], act['dayOfWeek'], act['timeStart'], act['timeEnd'])}
                        style={{ cursor: "pointer" }}>{act["timeStart"]}</td>
                    <td role="button" onClick={() => this.onClick(act['id'], act['dayOfWeek'], act['timeStart'], act['timeEnd'])}
                        style={{ cursor: "pointer" }}>{act["timeEnd"]}</td>
                    {/* <td><button className="circular ui icon button"
                        style={{ backgroundColor: "#FFABB6" }} type="button"
                        onClick={this.onDelete(act['id'])}>
                        <i className="minus icon"></i>
                    </button></td> */}
                </tr>
            )
        })
        return blocks
    }

    render() {
        return (
            this.createBlocks()
        )
    }
}

export default WorkHoursTr
