import React from "react";
import "semantic-ui-css/semantic.min.css";
import { Grid } from "semantic-ui-react";
import SideMenu from "./components/SideMenu";
import AppUnlogged from "./AppUnlogged";
import axios from 'axios'

function App() {
  if (!axios.defaults.headers.common["Authorization"]) {
    return (<AppUnlogged />);
  }

  return (
    <div style={{ height: "100%" }}>
      <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
        <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
          <SideMenu />
        </Grid.Column>
        <Grid.Column stretched width={14}>
          <div>
            <h1>HUNGER KILLER</h1>
            <hr />
            <p>
              Now you can use 100% of our web application.
            </p>
          </div>
        </Grid.Column>
      </Grid>
    </div>
  );
}

export default App;
