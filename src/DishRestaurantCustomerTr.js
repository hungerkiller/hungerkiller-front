import React from 'react'
import "./css/RestaurantTr.css";

class DishRestaurantCustomerTr extends React.Component {
    state = { userActivities: [] }
    constructor(props) {
        super(props);
        this.state = {
            bgColor: ""
        }
    }

    onClick(id) {
        var ordersIds = JSON.parse(localStorage.getItem("ordersIds"));
        const index = ordersIds.indexOf(id);
        if(index !== -1 ){
            ordersIds.splice(index, 1)
        }else{
            ordersIds.push(id);
        }
        localStorage.setItem("ordersIds", JSON.stringify(ordersIds));

        this.setState({
            refresh: ""
        })
    }

    createBlocks = () => {
        const blocks = this.props.dishes.map((act) => {
            return (
                <tr onClick={() => this.onClick(act['id'])} style={{backgroundColor: JSON.parse(localStorage.getItem("ordersIds")).indexOf(act['id']) === -1? "": "#ffe58a"}}>
                    <td>{act["name"]}</td>
                    <td>{act["description"]}</td>
                    <td>{act["price"]}</td>
                </tr>
            )
        })
        return blocks
    }

    render() {
        return (
            this.createBlocks()
        )
    }
}

export default DishRestaurantCustomerTr
