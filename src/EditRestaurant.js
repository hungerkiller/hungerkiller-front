import "semantic-ui-css/semantic.min.css";
import SideMenu from "./components/SideMenu";
import { Form, Grid } from "semantic-ui-react";
import React, { Component } from 'react';
import axios from 'axios'
import AppUnlogged from "./AppUnlogged";

class EditRestaurant extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            name: "",
            street: "",
            town: "",
            zipcode: "",
            description: "",
            openFromTime: "",
            openTillTime: "",
            phoneNumber: "",
            permissons: true,
            success: false,
            error: false
        }
    }

    componentDidMount() {
        const { id } = this.props.match.params
        var self = this;

        var axios = require('axios');
        var config = {
            method: 'get',
            url: 'http://localhost:8080/hungerkiller/v1/restaurant/id/' + id,
            headers: { }
        };

        axios(config)
            .then(function (response) {
                // console.log(JSON.stringify(response.data));
                self.setState({
                    name: response.data["name"],
                    street: response.data["address"]["street"],
                    town: response.data["address"]["town"],
                    zipcode: response.data["address"]["zipcode"],
                    description: response.data["description"],
                    openFromTime: response.data["openFromTime"],
                    openTillTime: response.data["openTillTime"],
                    phoneNumber: response.data["phoneNumber"]
                })
            })
            .catch(function (error) {
                // console.log(error);
            });
    }

    onSubmit = e => {
        const { id } = this.props.match.params
        var self = this;
        var axios = require('axios');
        var data = JSON.stringify({
            "id": id,
            "name": this.state.name,
            "address": {
                "street": this.state.street,
                "town": this.state.town,
                "zipcode": this.state.zipcode
            },
            "description": this.state.description,
            "openFromTime": this.state.openFromTime,
            "openTillTime": this.state.openTillTime,
            "phoneNumber": this.state.phoneNumber
        });

        var config = {
            method: 'put',
            url: 'http://localhost:8080/hungerkiller/v1/restaurant',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('Authorization')
            },
            data : data
        };

        axios(config)
            .then(function (response) {
                // console.log(JSON.stringify(response.data));
                self.setState({ success: true, error: false })
            })
            .catch(function (error) {
                self.setState({ error: true, success: false })
                // console.log(error);
            });
    }

    render() {
        if (!axios.defaults.headers.common["Authorization"]) {
            return (<AppUnlogged />);
        }
        return (
            <div style={{ height: "100%" }}>
                <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
                    <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
                        <SideMenu />
                    </Grid.Column>
                    <Grid.Column stretched width={14}>
                        <div>
                            <h1>Editing restaurant</h1>
                            <hr />
                            <Grid columns={2}>
                                <Grid.Column>
                                    <p style={{ display: this.state.success ? "block" : "none", color: "green" }}>SUCCESS!</p>
                                    <p style={{ display: this.state.error ? "block" : "none", color: "red" }}>Incorrect values.</p>
                                    <Form style={{ display: this.state.permissons ? "block" : "none" }} onSubmit={this.onSubmit}>
                                        <div style={{ backgroundColor: "#E9E9E9", borderRadius: 15, padding: 10}} >
                                           <div class="inline field" align="right">
                                                <label >Name:</label>
                                                <input  style={{ width: "80%" }} onChange={e => this.setState({ name: e.target.value })} value={this.state.name} />
                                            </div>
                                            <div className="inline field" align="right">
                                                <label>Phone number:</label>
                                                <input style={{ width: "80%" }} onChange={e => this.setState({phoneNumber: e.target.value})}
                                                       value={this.state.phoneNumber}/>
                                            </div>
                                            <div class="inline field" align="right">
                                                <label >Open Time:</label>
                                                <input style={{ width: "80%" }} onChange={e => this.setState({ openFromTime: e.target.value })} value={this.state.openFromTime} />
                                            </div>
                                            <div className="inline field" align="right">
                                                <label>Close Time:</label>
                                                <input style={{ width: "80%" }} onChange={e => this.setState({openTillTime: e.target.value})}
                                                       value={this.state.openTillTime}/>
                                            </div>
                                            <div className="inline field" align="right">
                                                <label>Street:</label>
                                                <input style={{ width: "80%" }} onChange={e => this.setState({street: e.target.value})}
                                                       value={this.state.street}/>
                                            </div>
                                            <div className="inline field" align="right">
                                                <label>Zipcode:</label>
                                                <input style={{ width: "80%" }} onChange={e => this.setState({zipcode: e.target.value})}
                                                       value={this.state.zipcode}/>
                                            </div>
                                            <div className="inline field" align="right">
                                                <label>Town:</label>
                                                <input style={{ width: "80%" }} onChange={e => this.setState({town: e.target.value})}
                                                       value={this.state.town}/>
                                            </div>
                                            <div className="inline field" style={{marginRight: 50, marginLeft: 50,}}>
                                                <label>Description:</label>
                                                <textarea onChange={e => this.setState({description: e.target.value})} value={this.state.description}/>
                                            </div>
                                        </div>
                                        <div align="right" style={{ marginTop: 10 }} >
                                            <button class="circular ui icon button" style={{ backgroundColor: "#FFABB6" }} type="reset" >
                                                <i class="minus icon"/>
                                            </button>
                                            <label style={{ marginRight: 10 }}> Reset </label>
                                            <button class="circular ui icon button" style={{ backgroundColor: "#B2E8C4" }} type='submit' >
                                                <i class="check icon"/>
                                            </button>
                                            <label>Save restaurant</label>
                                        </div>
                                    </Form>
                                </Grid.Column>
                            </Grid>
                        </div>
                    </Grid.Column>
                </Grid>
            </div >
        );
    }
}

export default EditRestaurant;
