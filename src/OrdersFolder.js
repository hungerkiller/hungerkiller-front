import "semantic-ui-css/semantic.min.css";
import SideMenu from "./components/SideMenu";
import { Grid } from "semantic-ui-react";
import React, { Component } from 'react';
import axios from 'axios'
import AppUnlogged from "./AppUnlogged";
import OrderCustomerTr from "./OrderCustomerTr";

class OrdersFolder extends Component {
    state = { orders: [], name: "", update: false, RT: null }

    componentDidMount() {
        var self = this;
        var axios = require('axios');

        var config = {
            method: 'get',
            url: 'http://localhost:8080/hungerkiller/v1/user/orders',
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('Authorization')
            }
        };

        axios(config)
            .then(function (response) {
                console.log(response.data);
                self.setState({ orders: response.data})
            })
            .catch(function (error) {
            });
    }

    componentDidUpdate() {
    }

    render() {
        if (!axios.defaults.headers.common["Authorization"]) {
            return (<AppUnlogged />);
        }
        return (
            <div style={{ height: "100%" }}>
                <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
                    <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
                        <SideMenu />
                    </Grid.Column>
                    <Grid.Column stretched width={14}>
                        <div>
                            <h1>Your orders</h1>
                            <hr />
                            <Grid columns={1}>
                                <Grid.Column>
                                    <table class="ui sortable celled table">
                                        <thead>
                                            <tr>
                                                <th>Order Id</th>
                                                <th>Restaurant name</th>
                                                <th>Creation time</th>
                                                <th>Status</th>
                                                <th>Deliverer Number</th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            <OrderCustomerTr orders={this.state.orders}/>
                                        </tbody>
                                    </table>
                                </Grid.Column>
                            </Grid>
                        </div>
                    </Grid.Column>
                </Grid>
            </div >
        );
    }
}

export default OrdersFolder;
