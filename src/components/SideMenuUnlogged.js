import React, { Component } from "react";
import { Menu, Container, Image, Button } from "semantic-ui-react";
import { Link } from 'react-router-dom'
import icon from "../logo.png";

export default class SideMenu extends Component {
    state = {};
    menuStyle = {
        border: 0,
        boxShadow: "none",
        fontSize: 20,
        backgroundColor: "#FFDC64",
        height: "100vh",
        width: "100%",
        "text-align": "center"
    };

    handleItemClick = (e, { name }) => this.setState({ activeItem: name });

    render() {
        const { activeItem } = this.state;
        return (
            <Menu vertical style={this.menuStyle} height={window.innerHeight}>
                <Container>
                    <Menu.Item>
                        <Link to="/"> <Image src={icon}></Image></Link>
                        <Menu.Menu>
                            <Menu.Item>
                                <Link to="/login"><Button style={{ backgroundColor: "#FFFFFF", width: "100%" }}>Log In</Button></Link>
                            </Menu.Item>
                            <Menu.Item>
                                <Link to="/register"><Button style={{ backgroundColor: "#FFFFFF", width: "100%" }}>Register</Button></Link>
                            </Menu.Item>
                            <br />
                            <Menu.Item>
                                <Menu.Header>Search:</Menu.Header>
                                <Menu.Menu>
                                    <Link to="/restaurants">
                                        <Menu.Item
                                            name="Restaurants"
                                            active={activeItem === "Restaurants"}
                                            onClick={this.handleItemClick}
                                        >
                                            Restaurants
                                        </Menu.Item>
                                    </Link>
                                    <Link to="/dishes">
                                        <Menu.Item
                                            name="Dishes"
                                            active={activeItem === "Dishes"}
                                            onClick={this.handleItemClick}
                                        >
                                            Dishes
                                        </Menu.Item>
                                    </Link>
                                </Menu.Menu>
                            </Menu.Item>
                        </Menu.Menu>
                    </Menu.Item>
                </Container>
            </Menu >
        );
    }
}
