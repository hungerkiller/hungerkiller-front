import React, { Component } from "react";
import {Menu, Container, Image, Button} from "semantic-ui-react";
import { Link } from 'react-router-dom'
import icon from "../logo.png";

export default class SideMenu extends Component {
  state = { name: "", lastName: "", permissons: false };
  menuStyle = {
    border: 0,
    boxShadow: "none",
    fontSize: 20,
    backgroundColor: "#FFDC64",
    height: "100vh",
    width: "100%"
  };

  componentDidMount() {
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });

  render() {
    const { activeItem } = this.state;

    return (
      <Menu vertical style={this.menuStyle} height={window.innerHeight}>
        <Container>
          <Menu.Item>
            <Link to="/"> <Image src={icon}></Image></Link>
            <Menu.Menu>
              <Menu.Item
                name="Logout"
                onClick={ e => { localStorage.clear(); window.location.reload(); }}
              >
                <Button style={{ backgroundColor: "#FFFFFF", width: "100%" }}>Logout</Button>
              </Menu.Item>
            </Menu.Menu>
          </Menu.Item>

          <Menu.Item>
            <Menu.Header>Lists:</Menu.Header>
            <Menu.Menu>
              <Link to="/restaurants">
                <Menu.Item
                  name="Restaurants"
                  active={activeItem === "Restaurants"}
                  onClick={this.handleItemClick}
                >
                  Restaurants
                </Menu.Item>
              </Link>
              <Link to="/dishes">
                <Menu.Item
                    name="Dishes"
                    active={activeItem === "Dishes"}
                    onClick={this.handleItemClick}
                >
                  Dishes
                </Menu.Item>
              </Link>
            </Menu.Menu>
          </Menu.Item>

          <Menu.Item style={{ display: localStorage.getItem("Role") !== "customer" ? "none" : "" }}>
            <Menu.Header>Customer:</Menu.Header>
            <Menu.Menu>
              <Link to="/orders">
                <Menu.Item
                    name="Orders"
                    active={activeItem === "Orders"}
                    onClick={this.handleItemClick}
                >
                  Orders history
                </Menu.Item>
              </Link>
            </Menu.Menu>
          </Menu.Item>

          <Menu.Item style={{ display: localStorage.getItem("Role") !== "restaurateur" ? "none" : "" }}>
            <Menu.Header>Restaurateur:</Menu.Header>
            <Menu.Menu>
              <Link to="/add-restaurant">
                <Menu.Item
                    name="AddRestaurants"
                    active={activeItem === "AddRestaurants"}
                    onClick={this.handleItemClick}
                >
                  Add new restaurant
                </Menu.Item>
              </Link>

              <Link to="/restaurant-orders">
                <Menu.Item
                    name="RestaurantOrders"
                    active={activeItem === "RestaurantOrders"}
                    onClick={this.handleItemClick}
                >
                  Orders
                </Menu.Item>
              </Link>
            </Menu.Menu>
          </Menu.Item>

          <Menu.Item style={{ display: localStorage.getItem("Role") !== "deliverer" ? "none" : "" }}>
            <Menu.Header>Deliverer:</Menu.Header>
            <Menu.Menu>
              <Link to="/deliverer">
                <Menu.Item
                  name="Deliverer info"
                  active={activeItem === "Deliverer"}
                  onClick={this.handleItemClick}
                >
                  Deliverer info
                </Menu.Item>
              </Link>

              <Link to="/job-offers">
                <Menu.Item
                  name="Job offers"
                  active={activeItem === "JobOffers"}
                  onClick={this.handleItemClick}
                >
                  Job offers
                </Menu.Item>
              </Link>

              <Link to="/current-orders">
                <Menu.Item
                  name="Current orders"
                  active={activeItem === "CurrentOrders"}
                  onClick={this.handleItemClick}
                >
                  Current orders
                </Menu.Item>
              </Link>
            </Menu.Menu>
          </Menu.Item>
        </Container>
      </Menu>
    );
  }
}
