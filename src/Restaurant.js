import "semantic-ui-css/semantic.min.css";
import SideMenu from "./components/SideMenu";
import { Form, Grid } from "semantic-ui-react";
import React, { Component } from 'react';
import axios from 'axios'
import AppUnlogged from "./AppUnlogged";
import { Link } from 'react-router-dom'
import DishRestaurantTr from "./DishRestaurantTr";
import DishRestaurantCustomerTr from "./DishRestaurantCustomerTr";


class Restaurant extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            name: "",
            street: "",
            town: "",
            zipcode: "",
            description: "",
            openFromTime: "",
            openTillTime: "",
            phoneNumber: "",
            dishes: [],
            permissons: true,
            success: false,
            error: false,
            customer: true,
            restaurateur: false,
            empty_order: false
        }
    }

    componentDidMount() {
        if(localStorage.getItem('Role') === "restaurateur") {
            this.setState({'restaurateur': true, 'customer': false})
        }
        const { id } = this.props.match.params
        var self = this;

        var axios = require('axios');
        var config = {
            method: 'get',
            url: 'http://localhost:8080/hungerkiller/v1/restaurant/id/' + id,
            headers: { }
        };

        axios(config)
            .then(function (response) {
                // console.log(JSON.stringify(response.data));
                self.setState({
                    name: response.data["name"],
                    street: response.data["address"]["street"],
                    town: response.data["address"]["town"],
                    zipcode: response.data["address"]["zipcode"],
                    description: response.data["description"],
                    openFromTime: response.data["openFromTime"],
                    openTillTime: response.data["openTillTime"],
                    phoneNumber: response.data["phoneNumber"],
                    dishes: response.data["dishes"]
                })
            })
            .catch(function (error) {
                // console.log(error);
            });
    }

    onSubmit = e => {
    }

    onDelete = e => {
        const { id } = this.props.match.params
        var self = this
        var axios = require('axios');

        var config = {
            method: 'delete',
            url: 'http://localhost:8080/hungerkiller/v1/restaurant/id/' + id,
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('Authorization')
            },
            "routes": {
                "cors": true
            }
        };

        axios(config)
            .then(function (response) {
                // console.log(JSON.stringify(response.data));
                self.setState({success: true})
            })
            .catch(function (error) {
                // console.log(error);
                self.setState({error: true})
            });
    }

    render() {
        if (!axios.defaults.headers.common["Authorization"]) {
            return (<AppUnlogged />);
        }

        var DT;
        if (localStorage.getItem('Role') === "restaurateur") {
            DT = <DishRestaurantTr dishes={this.state.dishes}/>
        }else{
            DT = <DishRestaurantCustomerTr dishes={this.state.dishes} />
        }

        return (
            <div style={{ height: "100%" }}>
                <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
                    <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
                        <SideMenu />
                    </Grid.Column>
                    <Grid.Column stretched width={14}>
                        <div>
                            <Link to="/restaurants">
                                <button class="ui labeled icon button">
                                    <i class="caret left icon"/> Back to list
                                    </button>
                            </Link>
                            <hr />
                            <Grid columns={1} style={{ marginTop: 15 }}>
                                <Grid.Column>
                                    <p style={{ display: this.state.success ? "block" : "none", color: "green" }}>Restaurant have been removed successfully.</p>
                                    <p style={{ display: this.state.error ? "block" : "none", color: "red" }}>Some errors in deleting the restaurant.</p>
                                    <Form style={{ display: this.state.success ? "none" : "block" }} onSubmit={this.onSubmit}>
                                        <div style={{ backgroundColor: "#E9E9E9", borderRadius: 15, padding: 20 }} >
                                            <Grid columns={2}>
                                                <Grid.Row>
                                                    <Grid.Column>
                                                        <div class="inline field">
                                                            <label >Name:</label>
                                                            <label style={{ color: "#918383" }}>{this.state.name}</label>
                                                        </div>
                                                        <div class="inline field">
                                                            <label >Phone number:</label>
                                                            <label style={{ color: "#918383" }}>{this.state.phoneNumber}</label>
                                                        </div>
                                                        <div class="inline field">
                                                            <label >Opening hours</label>
                                                            <label style={{ color: "#918383" }}>{this.state.openFromTime} - {this.state.openTillTime}</label>
                                                        </div>
                                                        <div class="inline field">
                                                            <label >Address:</label>
                                                            <label style={{ color: "#918383" }}>{this.state.street}, {this.state.zipcode} {this.state.town}</label>
                                                        </div>
                                                        <div className="inline field">
                                                            <label>Description:</label>
                                                            <label style={{
                                                                color: "#918383",
                                                            }}>{this.state.description}</label>
                                                        </div>
                                                    </Grid.Column>
                                                    <Grid.Column>
                                                        <div align="right" style={{ display: this.state.restaurateur ? "" : "none"}}>
                                                            <label style={{marginRight: 10}}> DELETE</label>
                                                            <button className="circular ui icon button"
                                                                    style={{backgroundColor: "#FFABB6"}} type="button"
                                                                    onClick={this.onDelete}>
                                                                <i className="minus icon"/>
                                                            </button><br/><br/>
                                                            <label style={{marginRight: 10}}>EDIT</label>
                                                            <Link to={"/edit-restaurant/" + this.props.match.params.id}>
                                                                <button className="circular ui icon button"
                                                                        style={{backgroundColor: "#B2E8C4"}}
                                                                        type='button'>
                                                                    <i className="check icon"/>
                                                                </button>
                                                            </Link>
                                                        </div>
                                                    </Grid.Column>
                                                </Grid.Row>
                                            </Grid>
                                        </div>
                                    </Form>
                                </Grid.Column>
                                <Grid.Column>
                                <div style={{ display: this.state.success ? "none" : "block" }}>
                                    <div align="right" style={{ display: this.state.restaurateur ? "" : "none", color: "red" }}>
                                        <Link to={"/restaurant-add-dish/" + this.props.match.params.id}>
                                            <button className="circular ui icon button"
                                                    style={{backgroundColor: "#B2E8C4"}}
                                                    type='button'>
                                                <i className="plus icon"/>
                                            </button>
                                            <label style={{marginRight: 10}}>Add dish</label>
                                        </Link>
                                    </div>
                                    <div align="right" style={{ display: this.state.customer ? "" : "none" }}>
                                        <Link to={"/new-order/" + this.props.match.params.id}>
                                            <button className="circular ui icon button"
                                                    style={{backgroundColor: "#B2E8C4"}}
                                                    type='button'>
                                                <i className="plus icon"/>
                                            </button>
                                            <label style={{marginRight: 10}}>Submit your order</label>
                                        </Link>
                                    </div>

                                    <h3>Dishes</h3>
                                    <hr/>
                                    <p style={{ display: this.state.customer ? "" : "none"}}> (Click on a dish to add it to your order) </p>
                                    <Grid columns={1}>
                                        <Grid.Column>
                                            <table className="ui sortable celled table">
                                                <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Description</th>
                                                    <th>Price</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                { DT }
                                                </tbody>
                                            </table>
                                        </Grid.Column>
                                    </Grid>
                                </div>
                                </Grid.Column>
                            </Grid>
                        </div>
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}

export default Restaurant;
