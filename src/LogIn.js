import React from "react";
import "semantic-ui-css/semantic.min.css";
import './css/LogIn.css'
import { Button, Form, Grid, Segment } from 'semantic-ui-react';
import SideMenuUnlogged from "./components/SideMenuUnlogged";
import history from "./history";


export default function LogIn() {
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [errorMessage, setErrorMessage] = React.useState("");

    function validateForm() {
        return email.length > 0 && password.length > 0;
    }

    function handleSubmit(event) {
        var axios = require('axios');
        var data = JSON.stringify({
            "email": email,
            "password": password
        });

        var config = {
            method: 'post',
            url: 'http://localhost:8080/hungerkiller/v1/user/login',
            headers: {
                'Content-Type': 'application/json'
            },
            data : data
        };

        axios(config)
            .then(function (response) {
                localStorage.setItem('Authorization', response.data["jwt"]);

                var axios2 = require('axios');
                var config2 = {
                    method: 'get',
                    url: 'http://localhost:8080/hungerkiller/v1/user',
                    headers: {
                        'Authorization': 'Bearer ' + localStorage.getItem('Authorization')
                    }
                };

                axios2(config2)
                    .then(function (response) {
                        // console.log(JSON.stringify(response.data));
                        localStorage.setItem('Role', response.data["role"]);
                        localStorage.setItem('Id', response.data["id"]);
                        localStorage.setItem("ordersIds", JSON.stringify([]));
                        history.replace("/");
                        window.location.reload();
                    })
                    .catch(function (error) {
                        // console.log(error);
                    });
            })
            .catch(function (error) {
                setErrorMessage("INCORRECT LOGIN DATA");
            });
    }

    return (
        <div style={{ height: "100%" }}>
            <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
                <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
                    <SideMenuUnlogged />
                </Grid.Column>
                <Grid.Column stretched width={14}>
                    <div>
                        <h1>Login page</h1>
                        <hr />
                        <div className="mainContent">
                            <div style={{ color: "red", textAlign: "center", padding: "1% 1% 1% 1%" }}>
                                {errorMessage}
                            </div>
                            <Grid centered columns={3}>
                                <Grid.Column>
                                    <Segment>
                                        <Form size="large" onSubmit={handleSubmit}>
                                            <Form.Input
                                                fluid
                                                icon="user"
                                                iconPosition="left"
                                                placeholder="Email"
                                                value={email}
                                                onChange={e => setEmail(e.target.value)}
                                            />
                                            <Form.Input
                                                fluid
                                                icon="lock"
                                                iconPosition="left"
                                                placeholder="Password"
                                                type="password"
                                                value={password}
                                                onChange={e => setPassword(e.target.value)}
                                            />
                                            <Button style={{ backgroundColor: "#FFDC64" }} disabled={!validateForm()} fluid size="large">
                                                LOG IN
                                            </Button>
                                        </Form>
                                    </Segment>
                                </Grid.Column>
                            </Grid>
                        </div>
                    </div>
                </Grid.Column>
            </Grid>
        </div>
    );
}
