import React from 'react'
import "./css/RestaurantTr.css";
import {Link} from "react-router-dom";

class DishRestaurantTr extends React.Component {
    state = { userActivities: [] }

    onClick(id) {
        window.location.href = "restaurant/" + id;
    }

    createBlocks = () => {
        const blocks = this.props.dishes.map((act) => {
            return (
                <tr>
                    <td><Link to={"/edit-dish/" + act["id"]}>{act["name"]}</Link></td>
                    <td><Link to={"/edit-dish/" + act["id"]}>{act["description"]}</Link></td>
                    <td><Link to={"/edit-dish/" + act["id"]}>{act["price"]}</Link></td>
                </tr>
            )
        })
        return blocks
    }

    render() {
        return (
            this.createBlocks()
        )
    }
}

export default DishRestaurantTr
