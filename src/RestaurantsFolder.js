import "semantic-ui-css/semantic.min.css";
import SideMenu from "./components/SideMenu";
import SideMenuUnlogged from "./components/SideMenuUnlogged";
import { Grid } from "semantic-ui-react";
import React, { Component } from 'react';
import axios from 'axios'
import RestaurantTr from './RestaurantTr';
import RestaurantTrNoLink from "./RestaurantTrNoLink";

class RestaurantsFolder extends Component {
    state = { restaurants: [], name: "", update: false, RT: null }

    componentDidMount() {
        var self = this;
        var axios = require('axios');

        var config = {
            method: 'get',
            url: 'http://localhost:8080/hungerkiller/v1/restaurant',
            headers: { }
        };

        axios(config)
            .then(function (response) {
                // console.log(response.data);
                self.setState({ restaurants: response.data})
            })
            .catch(function (error) {
            });
    }

    componentDidUpdate() {
        if (this.state.update) {
            var self = this;
            var axios = require('axios');

            var config = {
                method: 'get',
                url: 'http://localhost:8080/hungerkiller/v1/restaurant/name/' + self.state["name"],
                headers: {}
            };

            var config2 = {
                method: 'get',
                url: 'http://localhost:8080/hungerkiller/v1/restaurant',
                headers: { }
            };

            if(self.state["name"] !== "") {
                axios(config)
                    .then(function (response) {
                        self.setState({restaurants: response.data, update: false})
                    })
                    .catch(function (error) {
                    });
            }else{
                axios(config2)
                    .then(function (response) {
                        self.setState({restaurants: response.data, update: false})
                    })
                    .catch(function (error) {
                    });
            }
        }
    }


    render() {
        var RT;
        var Menu;
        if (!axios.defaults.headers.common["Authorization"]) {
            RT = <RestaurantTrNoLink restaurants={this.state.restaurants} />
            Menu = <SideMenuUnlogged/>
        }else{
            RT = <RestaurantTr restaurants={this.state.restaurants} />
            Menu = <SideMenu/>
        }

        return (
            <div style={{ height: "100%" }}>
                <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
                    <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
                        {Menu}
                    </Grid.Column>
                    <Grid.Column stretched width={14}>
                        <div>
                            <h1>Restaurants</h1>
                            <hr />
                            <Grid columns={1}>
                                <Grid.Column>
                                    <table class="ui sortable celled table">
                                        <thead>
                                            <tr>
                                                <th colSpan={5}>
                                                    <div class="ui category search">
                                                        <div class="ui icon input" style={{width: "100%"}}>
                                                            <input class="prompt" type="text" placeholder="Searching by name..." onChange={e => this.setState({ name: e.target.value, update: true })} />
                                                            <i class="search icon"/>
                                                        </div>
                                                        <div class="results"/>
                                                    </div>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>Name</th>
                                                <th>Opening hours</th>
                                                <th>Address</th>
                                                <th>Phone number</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                        <tbody >
                                            { RT }
                                        </tbody>
                                    </table>
                                </Grid.Column>
                            </Grid>
                        </div>
                    </Grid.Column>
                </Grid>
            </div >
        );
    }
}

export default RestaurantsFolder;
