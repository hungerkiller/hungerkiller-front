import "semantic-ui-css/semantic.min.css";
import SideMenuUnlogged from "./components/SideMenuUnlogged";
import { Form, Grid } from "semantic-ui-react";
import Select from 'react-select'
import React, { Component } from 'react';
import axios from 'axios'
import App from "./App";
import history from "./history";

const roles = [
    { value: 'customer', label: 'Customer' },
    { value: 'restaurateur', label: 'Restaurateur' },
    { value: 'deliverer', label: 'Deliverer' }
]

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = { show: false, success: false, error: false,
            name: "",
            surname:"",
            email:"",
            password:"",
            phoneNumber:"",
            street:"",
            town:"",
            zipcode:"",
            role:"customer"
            }
    }

    componentDidMount() {
    }

    onSubmit = e => {
        var self = this;
        var axios = require('axios');
        var data = JSON.stringify({
            "name": this.state.name,
            "surname": this.state.surname,
            "email": this.state.email,
            "password": this.state.password,
            "phoneNumber": this.state.phoneNumber,
            "deliveryAddress": {
                "street": this.state.street,
                "town": this.state.town,
                "zipcode": this.state.zipcode
            },
            "role": this.state.role
        });

        var config = {
            method: 'post',
            url: 'http://localhost:8080/hungerkiller/v1/user',
            headers: {
                'Content-Type': 'application/json'
            },
            data : data
        };

        axios(config)
            .then(function (response) {
                self.setState({ success: true, error: false })
                setTimeout(function() {
                    history.replace("/login");
                    window.location.reload();
                }, 1000)
            })
            .catch(function (error) {
                self.setState({ error: true, success: false })
            });
    }

    render() {
        if (axios.defaults.headers.common["Authorization"]) {
            return (<App />);
        }
        return (
            <div style={{ height: "100%" }}>
                <Grid style={{ height: "100%", padding: 0, margin: 0 }}>
                    <Grid.Column stretched width={2} style={{ padding: 0, margin: 0 }}>
                        <SideMenuUnlogged />
                    </Grid.Column>
                    <Grid.Column stretched width={14}>
                        <div>
                            <h1>New account</h1>
                            <hr />
                            <Grid columns={2}>
                                <Grid.Column>
                                    <p style={{ display: this.state.success ? "block" : "none", color: "green" }}>SUCCESS!</p>
                                    <p style={{ display: this.state.error ? "block" : "none", color: "red" }}>Incorrect values.</p>
                                    <Form onSubmit={this.onSubmit}>
                                        <div style={{ backgroundColor: "#e9e9e9", borderRadius: 15, padding: 10}} >
                                            <div className="inline field" align="right">
                                                <label>Email:</label>
                                                <input style={{width: "80%"}}
                                                       onChange={e => this.setState({email: e.target.value})}
                                                       placeholder=''/>
                                            </div>
                                            <div className="inline field" align="right">
                                                <label>Password:</label>
                                                <input type="password" style={{width: "80%"}}
                                                       onChange={e => this.setState({password: e.target.value})}
                                                       placeholder=''/>
                                               <br/><i>(Password must be strong.)</i>
                                            </div>
                                            <hr/>
                                            <div className="inline field" align="left">
                                                <label>Who you are?:</label>
                                                <Select options={roles} placeholder="Customer"
                                                        onChange={e => this.setState({role: e.value})}/>
                                            </div>
                                            <hr/>
                                            <div class="inline field" align="right">
                                                <label >Name:</label>
                                                <input  style={{ width: "80%" }} onChange={e => this.setState({ name: e.target.value })} placeholder='' />
                                            </div>
                                            <div className="inline field" align="right">
                                                <label>Surname:</label>
                                                <input style={{width: "80%"}}
                                                       onChange={e => this.setState({surname: e.target.value})}
                                                       placeholder=''/>
                                            </div>
                                            <div className="inline field" align="right">
                                                <label>Phone number:</label>
                                                <input style={{ width: "80%" }} onChange={e => this.setState({phoneNumber: e.target.value})}
                                                       placeholder=''/>
                                            </div>
                                            <div className="inline field" align="right">
                                                <label>Street:</label>
                                                <input style={{ width: "80%" }} onChange={e => this.setState({street: e.target.value})}
                                                       placeholder=''/>
                                            </div>
                                            <div className="inline field" align="right">
                                                <label>Zipcode:</label>
                                                <input style={{ width: "80%" }} onChange={e => this.setState({zipcode: e.target.value})}
                                                       placeholder=''/>
                                            </div>
                                            <div className="inline field" align="right">
                                                <label>Town:</label>
                                                <input style={{ width: "80%" }} onChange={e => this.setState({town: e.target.value})}
                                                       placeholder=''/>
                                            </div>
                                        </div>
                                        <div align="right" style={{ marginTop: 10 }} >
                                            <button class="circular ui icon button" style={{ backgroundColor: "#FFABB6" }} type="reset" >
                                                <i class="minus icon"></i>
                                            </button>
                                            <label style={{ marginRight: 10 }}> Reset </label>
                                            <button class="circular ui icon button" style={{ backgroundColor: "#B2E8C4" }} type='submit' >
                                                <i class="check icon"></i>
                                            </button>
                                            <label>Register</label>
                                        </div>
                                    </Form>
                                </Grid.Column>
                            </Grid>
                        </div>
                    </Grid.Column>
                </Grid>
            </div >
        );
    }
}

export default Register;

